Description
===========

Postgres 16 Docker image based on Alpine with a bunch of extensions:
1. [wal2json](https://github.com/eulerto/wal2json)
2. [timescaledb](https://github.com/timescale/timescaledb)
3. [postgis](https://github.com/postgis/docker-postgis)
4. <sup>[1](#pgrx)</sup> [pg_jsonschema](https://github.com/supabase/pg_jsonschema)
5. [pg-safeupdate](https://github.com/eradman/pg-safeupdate)
6. [pg_hashids](https://github.com/iCyberon/pg_hashids)

<sup  id="pgrx">1</sup> Some extensions use [pgrx](https://github.com/pgcentralfoundation/pgrx)

Build
=====

Run on a system with Docker installed:
```bash
docker build -t some_tag .
```

#### Postgres version

It may possible to build an image for a lower postgres version by supplying an argument `PG_VERSION`, but this is untested. Example: 
```bash
docker build --build-arg PG_VERSION=15 -t some_tag .
```

#### Clean run

Use `--no-cache` for a clean build. Generally useful when a build with the cache fails.
```bash
docker build --no-cache -t some_tag .
```

License
=======

    Copyright 2024 Sinny Supernova

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.