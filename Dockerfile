ARG PG_VERSION=16

FROM postgres:${PG_VERSION}-alpine as base

FROM base as cmake-builder
RUN apk add --no-cache build-base git clang15 llvm15

# 1. wal2json
# https://github.com/eulerto/wal2json
FROM cmake-builder as wal2json-builder
RUN git clone https://github.com/eulerto/wal2json
WORKDIR /wal2json
RUN USE_PGXS=1 make && make install

# 2. timescaledb
# https://github.com/timescale/timescaledb
FROM timescale/timescaledb:latest-pg${PG_VERSION} as timescale

# self-built
# FROM base as timescale-builder
# # only major.minor version
# ENV TIMESCALE_VERSION=2.13
# RUN apk add --no-cache build-base git cmake openssl-dev krb5-dev
# RUN git clone https://github.com/timescale/timescaledb
# WORKDIR /timescaledb
# RUN git checkout $(git tag -l "${TIMESCALE_VERSION}.*" --sort=-v:refname | grep -E "${TIMESCALE_VERSION}\.[0-9]+$" | head -n 1)
# RUN ./bootstrap -DCMAKE_BUILD_TYPE=RelWithDebInfo -DREGRESS_CHECKS=OFF -DTAP_CHECKS=OFF -DGENERATE_DOWNGRADE_SCRIPT=ON -DWARNINGS_AS_ERRORS=OFF -DPROJECT_INSTALL_METHOD="docker"
# RUN cd build && make && make install

# 3. postgis
# https://github.com/postgis/docker-postgis
FROM postgis/postgis:${PG_VERSION}-3.4-alpine as postgis

# pgrx
# https://github.com/pgcentralfoundation/pgrx
FROM base as pgrx
RUN apk add --no-cache curl gcc musl-dev openssl-dev
WORKDIR /etc/rust
ENV HOME="/etc/rust"\
    PATH=/etc/rust/.cargo/bin:$PATH \
    CARGO_TARGET_DIR=/tmp/cargo-artifacts
RUN chown postgres:postgres /etc/rust
RUN \
    --mount=type=cache,target=/etc/rust \ 
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path --profile minimal && \
    rustup --version && \
    rustc --version && \
    cargo --version
RUN \
    --mount=type=cache,target=/etc/rust \ 
    rustup component add rustfmt
ENV OPENSSL_DIR=/usr \
    RUSTFLAGS="-C target-feature=-crt-static"
RUN \
    --mount=type=cache,target=/etc/rust \ 
    --mount=type=cache,target=/tmp/cargo-artifacts \
    cargo install cargo-pgrx --version 0.11.2 --locked
RUN \
    --mount=type=cache,target=/etc/rust \ 
    --mount=type=cache,target=/tmp/cargo-artifacts \
    cargo pgrx init --pg${PG_VERSION%%.*} $(which pg_config)

# 4. pg_jsonschema
# https://github.com/supabase/pg_jsonschema
FROM pgrx as pgrx-pg_jsonschema
RUN apk add --no-cache git build-base clang15
RUN \
    --mount=type=cache,target=/etc/rust \ 
    git clone https://github.com/supabase/pg_jsonschema pg_jsonschema || (cd pg_jsonschema && git pull origin master)
RUN \
    --mount=type=cache,target=/etc/rust \ 
    --mount=type=cache,target=/tmp/cargo-artifacts \
    cd pg_jsonschema && cargo pgrx install --release

# 5. pg-safeupdate
# https://github.com/eradman/pg-safeupdate
FROM cmake-builder as pg-safeupdate
RUN git clone https://github.com/eradman/pg-safeupdate
WORKDIR /pg-safeupdate
RUN gmake && gmake install

# 6. pg_hashids
# https://github.com/iCyberon/pg_hashids
FROM cmake-builder as pg-hashids
RUN git clone https://github.com/iCyberon/pg_hashids
WORKDIR /pg_hashids
RUN USE_PGXS=1 make && make install

# Final image
FROM base
# 1. wal2json
COPY --from=wal2json-builder /wal2json/wal2json.so /usr/local/lib/postgresql
RUN sed -i 's/^wal_level.*/wal_level = logical/' /usr/local/share/postgresql/postgresql.conf.sample
RUN sed -i 's/^max_slot_wal_keep_size.*/max_slot_wal_keep_size = 1024/' /usr/local/share/postgresql/postgresql.conf.sample
# 2. timescaledb
COPY --from=timescale /usr/local/lib/postgresql/timescaledb*.so /usr/local/lib/postgresql
COPY --from=timescale /usr/local/share/postgresql/extension/timescaledb--*.sql /usr/local/share/postgresql/extension/
COPY --from=timescale /usr/local/share/postgresql/extension/timescaledb.control /usr/local/share/postgresql/extension/
RUN sed -r -i "s/[#]*\s*(shared_preload_libraries)\s*=\s*'(.*)'/\1 = 'timescaledb,\2'/;s/,'/'/" /usr/local/share/postgresql/postgresql.conf.sample
# 3. postgis
RUN apk add --no-cache geos proj json-c protobuf-c
COPY --from=postgis /usr/local/lib/postgresql/postgis*.so /usr/local/lib/postgresql
COPY --from=postgis /usr/local/share/postgresql/extension/postgis*.sql /usr/local/share/postgresql/extension/
COPY --from=postgis /usr/local/share/postgresql/extension/postgis*.control /usr/local/share/postgresql/extension/
COPY --from=postgis /docker-entrypoint-initdb.d/10_postgis.sh /docker-entrypoint-initdb.d/003_install_postgis.sh
COPY --from=postgis /usr/local/bin/update-postgis.sh /usr/local/bin
# 4. pg_jsonschema
COPY --from=pgrx-pg_jsonschema /usr/local/share/postgresql/extension/pg_jsonschema*.sql /usr/local/share/postgresql/extension/
COPY --from=pgrx-pg_jsonschema /usr/local/share/postgresql/extension/pg_jsonschema.control /usr/local/share/postgresql/extension/
COPY --from=pgrx-pg_jsonschema /usr/local/lib/postgresql/pg_jsonschema.so /usr/local/lib/postgresql
# 5. pg-safeupdate
COPY --from=pg-safeupdate /usr/local/lib/postgresql/safeupdate.so /usr/local/lib/postgresql
# 6. pg_hashids
COPY --from=pg-hashids /usr/local/share/postgresql/extension/pg_hashids*.sql /usr/local/share/postgresql/extension/
COPY --from=pg-hashids /usr/local/share/postgresql/extension/pg_hashids.control /usr/local/share/postgresql/extension/
COPY --from=pg-hashids /usr/local/lib/postgresql/pg_hashids.so /usr/local/lib/postgresql

# initdb scripts
COPY docker-entrypoint-initdb.d/* /docker-entrypoint-initdb.d/

# uncomment for quick testing
# ENV POSTGRES_PASSWORD=whatever

# uncomment and replace _image_ for intermediate image file inspection
# COPY --from=_image_ /usr/local/share/postgresql/extension/ /check/ext/
# COPY --from=_image_ /usr/local/lib/postgresql/ /check/lib/