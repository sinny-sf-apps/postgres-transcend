#!/bin/bash

create_sql=$(mktemp)

cat <<EOF >${create_sql}
CREATE EXTENSION IF NOT EXISTS pg_jsonschema CASCADE;
EOF

# create extension pg_jsonschema in initial databases
psql -U "${POSTGRES_USER}" postgres -f "${create_sql}"
psql -U "${POSTGRES_USER}" template1 -f "${create_sql}"

if [ "${POSTGRES_DB:-postgres}" != 'postgres' ]; then
    psql -U "${POSTGRES_USER}" "${POSTGRES_DB}" -f "${create_sql}"
fi